//
// Created by evgenii on 29.09.2021.
//

#ifndef SPOLAB_COMMAND_EXECUTOR_H
#define SPOLAB_COMMAND_EXECUTOR_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "../../protocol/message.pb-c.h"
#include "../../graph_module/graph_struct.h"
#include "../../structs//linked_list.h"

void command_create(Common__Request *request, Common__Response *response);

void command_delete(Common__Request *request, Common__Response *response);

void command_match(Common__Request *request, Common__Response *response);

void command_set(Common__Request *request, Common__Response *response);

#endif //SPOLAB_COMMAND_EXECUTOR_H
